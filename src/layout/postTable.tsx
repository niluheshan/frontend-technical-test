import { FunctionComponent, useEffect, useState } from "react";
import { getAllPosts } from "../api";
import { Post } from "../app/PostModel";
import Headline from "../components/headline";
import Loading from "../components/loading";
import Pagination from "../components/pagination";
import SizeSpinner from "../components/sizeSpinner";
import TableComponent from "../components/table";
import useFetch from "../customHook/useFetch";

interface PostTableProps {}

export const PostTable: FunctionComponent<PostTableProps> = () => {
  const [page, setPage] = useState(1);
  const [limit, setlimit] = useState<number>(10);
  const [currentData, setcurrentData] = useState<Post[]>([]);
  const { data, error, loading } = useFetch<Post[]>(
    "https://jsonplaceholder.typicode.com/posts",
    undefined
  );
  console.log(page, "@@@");
  console.log(limit, "@@@");
  console.log(currentData, "@@@");
  console.log(data, "@@@");

  useEffect(() => {
    if (data && page == 1) {
      setcurrentData(data.slice(0, 10));
    }
  });

  // const [data, setdata] = useState<Post[]>([]);
  // useEffect(() => {
  //   let response = getAllPosts<Post[]>();
  //   response
  //     .then((res) => {
  //       let posts = res.data.map(
  //         (p) => new Post(p.id, p.userId, p.title, p.body)
  //       );
  //       setdata(posts);
  //       pageHandle();
  //     })
  //     .catch((err) => {
  //       setdata([]);
  //     });
  // }, []);

  const reset = () => {
    setPage(1);
    setcurrentData([]);
    // setdata([]);
    setlimit(5);
  };
  const pageHandle = () => {
    let to = limit * page;
    let from = to - limit;
    let curr = data?.slice(from, to);
    setcurrentData(curr || []);
  };

  const handleClick = (event: number) => {
    setPage(event);
  };

  const handleSpinner = (event: number) => {
    setlimit(event);
  };

  useEffect(() => {
    pageHandle();
  }, [page]);

  useEffect(() => {
    pageHandle();
  }, [limit]);

  return (
    <>
      <Headline page={page} limit={limit} data={data||[]}/>
      <TableComponent
        data={currentData}
        properties={[
          { key: "id", label: "id" },
          { key: "title", label: "title" },
          { key: "body", label: "body" },
        ]}
      />
      <Pagination
        active={page}
        limit={limit}
        data={data || ([] as any)}
        handle={handleClick}
      />
      <SizeSpinner count={limit} handle={handleSpinner} />
      {loading ? <Loading /> : ""}
    </>
  );
};

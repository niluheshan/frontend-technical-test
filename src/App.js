import "bootstrap/dist/css/bootstrap.min.css";
import "./App.scss";
import "./styles/headline.scss"
import { PostTable } from "./layout/postTable";

function App() {
  return (
    <div className="App">
      <PostTable />
    </div>
  );
}

export default App;

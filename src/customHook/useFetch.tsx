import axios from "axios";
import { useEffect, useState } from "react";

const useFetch= <T,>(url: string, options: undefined) => {
  const [data, setdata] = useState<T|null>(null);
  const [error, seterror] = useState(null);
  const [loading, setloading] = useState(false);

  useEffect(() => {
    let isMounted = true;
    setloading(true);

    axios
      .get<T>(url, options)
      .then((res) => {
        if (isMounted) {
          setdata(res.data);
          seterror(null);
        }
      })
      .catch((err) => {
        if (isMounted) {
          seterror(err);
          setdata(null);
        }
      })
      .finally(() => {
        if (isMounted) {
          setloading(false);
        }
      });
  }, [url, options]);

  return { data, error, loading };
};

export default useFetch;

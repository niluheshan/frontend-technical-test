import { FunctionComponent, useState } from "react";

interface SizeSpinnerProps {
  handle:Function
  count:number
}

const SizeSpinner: FunctionComponent<SizeSpinnerProps> = ({handle,count}) => {
  const [value, setvalue] = useState<number>(count);

  const handleChange = () => {
      setvalue(value+1)
      handle(value+1);
  };

  return (
    <input
      title="Size Spinner"
      type="number"
      min="0"
      max="20"
      value={`${value}`}
      onChange={handleChange}
    />
  );
};

export default SizeSpinner;

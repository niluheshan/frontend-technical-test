import { FunctionComponent } from "react";

interface HeadlineProps {
  data:any[];
  limit:number;
  page:number;
}

const Headline: FunctionComponent<HeadlineProps> = ({data,page,limit}) => {

  return (
    <>
      <div className="headline">
        <h3>{`${data.length} Total`}</h3>
        <h3>{`${page}/${data.length/limit}`}</h3>
      </div>
    </>
  );
};

export default Headline;

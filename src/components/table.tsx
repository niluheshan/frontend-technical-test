import * as React from "react";
import { Component } from "react";
import { Table } from "react-bootstrap";

interface TableProps<T> {
  data: T[];
  properties: {
    key: keyof T;
    label: string;
  }[];
}

const TableComponent = <T,>(props: TableProps<T>) => {
  const { data, properties } = props;
  console.log(data, ">>>>>");
  const propertyMapper = (rowData: T) => {
    return (
      <tr>
        {properties.map((property) => {
          return <td key={String(property.key)}>{rowData[property.key]}</td>;
        })}
      </tr>
    );
  };

  const renderBody = () => {
    return data.map((post) => {
      return propertyMapper(post);
    });
  };

  return (
    <Table className="tableStyles">
      <thead>
        <tr>
          <th>#</th>
          <th>Title</th>
          <th>Body</th>
        </tr>
      </thead>
      <tbody>
        {renderBody()}
      </tbody>
    </Table>
  );
};

export default TableComponent;

import { FunctionComponent, useState } from "react";
import { Button } from "react-bootstrap";

interface PaginationProps {
  data: Object[];
  limit: number;
  active: number;
  handle:Function;
}

const Pagination: FunctionComponent<PaginationProps> = (props) => {
  const { data, limit, active, handle } = props;

  const handleClick = (event: number) => {
    handle(event)
  };

  let items: JSX.Element[] = [];
  for (let index = 1; index <= data.length / limit; index++) {
    items.push(
      <Button
        key={index}
        variant="outline-primary"
        active={active === index}
        onClick={() => handleClick(index)}
      >
        {index}
      </Button>
    );
  }

  return <>{items}</>;
};

export default Pagination;

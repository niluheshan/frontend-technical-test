import axios from "axios";

const API_BASE = "https://jsonplaceholder.typicode.com";

export const getAllPosts = async <T>() => {
  let response = await axios.get<T>(`${API_BASE}/posts`);
  return response;
};
